//
//  User.swift
//  ToDoFIREBASE
//
//  Created by Jhonny Green on 06.02.2020.
//  Copyright © 2020 Test Lessons. All rights reserved.
//

import Foundation
import Firebase

struct User {
    
    let uid: String
    let email: String
    
    init(user: UserInfo) {
        self.uid = user.uid
        self.email = user.email!
    }
    
}
