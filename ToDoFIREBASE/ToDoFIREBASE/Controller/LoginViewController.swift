//
//  ViewController.swift
//  ToDoFIREBASE
//
//  Created by Jhonny Green on 05.02.2020.
//  Copyright © 2020 Test Lessons. All rights reserved.
//

import UIKit
import Firebase


class LoginViewController: UIViewController {
    
    let segueIdentidier = "tasksSegue"
    var ref: DatabaseReference!
    
    
    
    @IBOutlet weak var warningLabel: UILabel!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference(withPath: "users")
        warningLabel.alpha = 0
        
        Auth.auth().addStateDidChangeListener {[weak self] (auth, user) in
            if user != nil {
                self?.performSegue(withIdentifier: (self?.segueIdentidier)!, sender: nil)
            }
        }
        
    }
    override func viewWillAppear(_ animated: Bool) { // почему то не работает
        super.viewWillAppear(animated)
        
        
        emailTF.text = ""
        passwordTF.text = ""
    }
    
    func displayWarningLabel(withText text: String) {
        warningLabel.text = text
        UIView.animate(withDuration: 3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseInOut, animations: { [weak self] in
            self?.warningLabel.alpha = 1
        }) { [weak self] complete in
            self?.warningLabel.alpha = 0
        }
    }
    
    @IBAction func loginTappedAvtion(_ sender: Any) {
        guard let email = emailTF.text, let password = passwordTF.text, email != "", password != ""
            else {
                displayWarningLabel(withText: "info is incorrect")
                return
        }
        
        Auth.auth().signIn(withEmail: email, password: password) { [weak self] (user, error) in
            if error != nil {
                self?.displayWarningLabel(withText: "Error occured")
                return
            }
            
            if user != nil {
                self?.performSegue(withIdentifier: "tasksSegue", sender: nil)
                return
            }
            self?.displayWarningLabel(withText: "No such user")
        }
        
        
    }
    @IBAction func registerTappedAction(_ sender: Any) {
        guard let email = emailTF.text, let password = passwordTF.text, email != "", password != ""
            else {
                displayWarningLabel(withText: "info is incorrect")
                return
        }
        Auth.auth().createUser(withEmail: email, password: password) {(user, error) in
            
            if error == nil {
                if user != nil {
                    
                } else {
                    print("user is not created")
                }
            } else {
                print(error?.localizedDescription ?? "")
            }
        }
    }
    
}
/*
 guard error == nil, user != nil else {
 print(error!.localizedDescription)
 return
 }
 let userRef = self.ref.child((user?.uid)!)
 userRef?.setValue(["email": user.email])
 }
 */


